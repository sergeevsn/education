from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class TaskCategory(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None)

    def __str__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категория'



class UserExtendent(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    STUDENT = 'ST'
    EMPLOYEER = 'EM'
    CATEGORY_CHOICES = (
        (STUDENT, 'Студент'),
        (EMPLOYEER, 'Работодатель'),
    )

    category = models.CharField(max_length=40,
                                choices=CATEGORY_CHOICES, blank=True)

    name = models.TextField(max_length=100, blank=True)
    desc = models.TextField(max_length=500, blank=True)
    location = models.TextField(max_length=250, blank=True)
    phone = models.CharField(max_length=12, blank=True)
    email = models.EmailField(blank=True)
    education = models.TextField(max_length=250, blank=True)
    kurs = models.IntegerField(max_length=1, blank=True, default=0)


# @receiver(post_save, sender=User)
# def create_userext(sender, instance, created, **kwargs):
#     if created:
#         UserExtendent.objects.create(user=instance)
#
# @receiver(post_save, sender=User)
# def save_userext(sender, instance, **kwargs):
#     instance.userext.save()

@receiver(post_save, sender=User)
def new_user(sender, instance, created, **kwargs):
    if created:
        UserExtendent.objects.create(user=instance)
    instance.userextendent.save()


class Comment(models.Model):
    post = models.ForeignKey('blog.Post', related_name='comments')
    author = models.CharField(max_length=200)
    approve_comment = models.BooleanField(default=False)
    text = models.TextField()
    add_date = models.DateTimeField(default=timezone.now)

    def approve(self):
        self.approve_comment = True
        self.save()

    def __str__(self):
        return self.text



class Post(models.Model):
    ARCHITECT = 'AR'
    VIDEO = 'VI'
    MARKETING = 'MA'
    MUSIC = 'MU'
    DESIGN = 'DE'
    PROGRAMM = 'PR'
    PHOTO = 'PH'
    ENGINEER = 'EN'
    MANAGE = 'MG'

    CATEGORY_CHOICES = (
        (ARCHITECT, 'Архитектура'),
        (VIDEO, 'Видео'),
        (MARKETING, 'Маркетинг'),
        (MUSIC, 'Музыка'),
        (DESIGN, 'Дизайн'),
        (PROGRAMM, 'IT и программирование'),
        (PHOTO, 'Фотография'),
        (ENGINEER, 'Инженерия'),
        (MANAGE, 'Менеджмент'),
    )

    excategory = models.CharField(max_length=40,
                                choices=CATEGORY_CHOICES, blank=True, null=True,)

    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    category = models.ForeignKey(TaskCategory, blank=True, null=True, default=None)
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)
    executor = models.ManyToManyField(UserExtendent)
    isactive = models.BooleanField(default=True, blank=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()


    def __str__(self):
        return self.title