from django import forms
from django.contrib.auth.models import User
from .models import Post, Comment
from .models import UserExtendent
from django.contrib.auth.forms import UserCreationForm

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'excategory', 'text',)

class SignUpForm(UserCreationForm):

    desc = forms.CharField(max_length=500)
    location = forms.CharField(max_length=250)
    phone = forms.CharField(max_length=13)
	adress = forms.AddresField(max_length=2300
    class Meta:
        model = User
        fields = ('username', 'desc', 'location', 'phone', 'email', 'password1', 'password2', )

class CommentsForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('author', 'text',)